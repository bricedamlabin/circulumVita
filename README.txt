Il est temps de travailler la mise en page de mon cv! 
Pour le moment, nos pages étaient très, comment dire... verticales. 
Je veux travailler le sens de lecture et profiter de la puissance de la mise en page CSS pour nos pages !
Je vais structurer ici la page comme ceci :
A gauche, un liseré (purement décoratif, mais je peux aussi mettre des informations à l'intérieur)
A droite, le contenu de votre CV, qui contiendra à l'intérieur, de gauche à droite les sections suivantes :
-> Mon expérience
-> Mes compétences
-> Ma formation
